package chatbroadcastrmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author aluno
 */
public interface Service extends Remote {
    
    public void enviar(String msg) throws RemoteException;
    public String escutar() throws RemoteException;
  
   
}
