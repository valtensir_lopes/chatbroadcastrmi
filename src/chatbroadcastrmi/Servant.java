package chatbroadcastrmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author aluno
 */
public class Servant extends UnicastRemoteObject implements Service{
    
    private String msg;
    public Servant() throws RemoteException{
        super();
        msg = "";
    }

    
    
    
    @Override
    public void enviar(String msg) throws RemoteException {
        this.msg = msg;
    }
    
    @Override
    public String escutar() throws RemoteException {
            return msg;
    }
    
}
