package chatbroadcastrmi;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Scanner;

/**
 *
 * @author aluno
 */
public class Client  {

    public static Service service; 
    public static void main(String[] args) throws NotBoundException, MalformedURLException, RemoteException, UnknownHostException {
        
        service = (Service) Naming.lookup("rmi://localhost:5099/conectionBroadcast");
        new Thread(recebe).start();
        new Thread(envia).start();
        
    }
    
    private static Runnable envia =  new Runnable(){
        Scanner s=new Scanner(System.in);
        String msg="";
        @Override
        public void run(){
            try{
                System.out.println("Digite mensagens para enviar a outros clientes, para sair, digite 0");
                while(true){
                msg = s.nextLine();
                if (msg.equals("0")){
                    System.exit(0);
                }
                service.enviar(msg);
                }
            } catch (RemoteException e){}
        }
    };
    
    
    private static Runnable recebe = new Runnable(){
        String msg = "";
        @Override
        public void run() {
           try {
               while (true){
                   if (!"".equals(service.escutar()) && !msg.equals(service.escutar())){
                       System.out.println("Mensagem do servidor: " + service.escutar());
                       msg = service.escutar();
                   }
               }
           } catch (RemoteException e){}
        } 
    };
    
}
